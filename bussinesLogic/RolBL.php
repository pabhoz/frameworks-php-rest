<?php

class RolBL{

    public function getAll(){
          return Rol::getAll();
    }
    public function getObjById($id){
            return Rol::getById($id);
    }
    public function load($arr){
        return Rol::instanciate($arr); 
    }
}
