<?php

class UsersBL{

    public function getAll(){
          return Usuario::getAll();
    }
    public function getObjById($id){
            return Usuario::getById($id);
    }
    public function load($arr){
        $arr["password"] = Hash::create($arr["password"]);
        return Usuario::instanciate($arr); 
    }
}
