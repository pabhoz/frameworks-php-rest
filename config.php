<?php

  $_PROTOCOL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
  define('ROOT', $_PROTOCOL.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
  define('MODELS', './models/');
  define('BL','./bussinesLogic/');
  define('LOCAL_SERVER',false);

  define('URL', ROOT . 'services/');
  define('LIBS', 'libs/');

  define('HASH_KEY', '1234');
  define('HASH_SECRET', '5678');
  define('HASH_PASSWORD_KEY', '9123');
  define('SECRET_WORD', 'scrtwrd');

  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'mydb');
  define('DB_TYPE', 'mysql');
