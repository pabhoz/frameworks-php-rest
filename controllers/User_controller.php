<?php

class User_controller extends Controller {

	function __construct() {
		parent::__construct();
	}

        public function getUser(){
            $usrs = UsersBL::getAll();
            print(json_encode($usrs));
        }

        public function postUser(){
						//header('Content-Type: text/html; charset=utf-8');
            //print_r($_POST);
            $usr = UsersBL::load($_POST);//Cargar usuario
            $rol = RolBL::getObjById(2);//cargar rol de usuario

            $usr->has_one("Rol",$rol);//Crear relación entre usuario y rol
            print_r(json_encode($usr->create(),1));
        }

        public function postFriend(){

            $idUser = $_POST["id_user"];
            $idFriend = $_POST["id_friend"];

            $usr = UsersBL::getObjById($idUser);//Cargar usuario
            $frnd = UsersBL::getObjById($idFriend);//Cargar amigo

            $usr->has_many("Amigos",$frnd);

            print_r(json_encode($usr->update(),1));
        }

}
